<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S2 Activity</title>
</head>
<body>

<h1>Divisible by 5</h1>
<p><?= printDivisibleOfFive(1000); ?></p> 

<h1>Array Manipulation</h1>
<p><?php array_push($students, 'John Smith')?></p>
<p><?php var_dump($students)?>
<p><?= count($students)?>
<p><?php array_push($students, 'Jane Smith')?></p>
<p><?php var_dump($students)?>
<p><?= count($students)?>
<p><?php array_shift($students)?>
<p><?php var_dump($students)?>
<p><?= count($students)?>

    
</body>
</html>