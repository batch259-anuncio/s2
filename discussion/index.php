<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SO2: Repetition Control Structures and Array Manipulation</title>
</head>

<body>
    <h1>Repetition Control Structures</h1>
    <h2>While Loop</h2>
    <?php whileLoop() ?>

    <h2>Do-While Loop</h2>
    <?php doWhileLoop(); ?>

    <h2>For Loop</h2>
    <?php forLoop(); ?>

    <h2>Continue and Break Statement</h2>
    <?php modifiedForLoop(); ?>

    <h2>Mini Activity</h2>
    <?php whileLoopActivity(); ?>

    <!-- Array Manipulation -->
    <h1>Array Manipulation</h1>

    <h2>Types of Arrays</h2>

    <h3>Simple Array</h3>

    <!-- 
        foreach
            - This loop only works in Array.

        Syntax:
            foreach($array as $value/$element)
            // code to be executed
     -->

    <ul>
        <!-- php codes/statements can be breakdown using the php tags -->
        <?php foreach ($computerBrands as $brand) : ?>
            <!-- PHP includes a short hand for "php echo tag" -->
            <li><?= $brand; ?></li>
        <?php endforeach ?>
    </ul>

    <h3>Two / Multi-Dimensional Array</h3>
    <ul>
        <?php
        foreach ($heroes as $team) :
            foreach ($team as $member) : ?>

                <li> <?= $member ?> </li>

        <?php endforeach; endforeach; ?>
     
    </ul>

    <h3>Multi-Dimensional Array</h3>
    <ul>
        <?php
        foreach ($ironManPowers as $label => $powerGroup) {
            foreach ($powerGroup as $power) {
        ?>

                <li> <?= "$label: $power " ?> </li>

        <?php
            }
        }
        ?>
    </ul>


    <h2>Array Functions</h2>

    

    <pre>
        <?php print_r($computerBrands); ?>
    </pre>
    
    <h3>Sorting in Ascending Order</h3>
    <pre><?php print_r($sortedBrands); ?></pre>

    <h3>Sorting in Descinding Order</h3>
    <pre><?php print_r($reverseSortedBrands); ?></pre>



</body>
</html>
